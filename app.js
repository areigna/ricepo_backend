//dependencies
var http = require('http');
var https = require('https');
var express = require('express');
var passwordHash = require('password-hash');
var shortId = require('shortid');
//custom modules preload
var data = require('data');//preload the db connection
var paramCheck = require('routes/paramCheck');
var call = require('routes/call');
var error = require('routes/error');
var user = require('routes/user');
var rest = require('routes/restaurant');
var food = require('routes/food');
var order = require('routes/order');

//new instance of modules
var app = express();

//--------------------------------

//middlewares
app.use(express.logger('short'));
app.use(function(req, res, next){
	console.log();
	console.log(new Date());
	next();
});
app.use(express.bodyParser());


//CORS
app.all('*', function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
	next();
});


//index reponse
app.all('/', function(res, res){
	res.end('You are reaching Ricepo OK.');
});

//authenticate
app.all('/user/*',user.authenticate);


//getting response

//restaurant
app.all('/getCitiesByCountry',paramCheck.check(null,{country:''}));
app.all('/getCitiesByCountry', rest.getCitiesByCountry);

app.all('/getRestsByCity',paramCheck.check(null,{city:''}));
app.all('/getRestsByCity', rest.getRestsByCity);

app.all('/getLoginRest',paramCheck.check(null,{city:''}));
app.all('/getLoginRest', rest.getLoginRest);

app.all('/loginRest',paramCheck.check({rest_id: '', password: ''}));
app.all('/loginRest', rest.loginRest);

app.all('/registerRest',paramCheck.check({rest_id: '', password: ''}));
app.all('/registerRest', rest.registerRest);

app.all('/getFoodsByRest',paramCheck.check({rest_id:''}));
app.all('/getFoodsByRest', food.getFoodsByRest);

app.post('/addOrder', order.addOrder);

//app.all('/pushOrder',paramCheck.check({rest_id:'', order_id: ''}));
//app.all('/pushOrder', order.pushOrder);

app.all('/getOrder',paramCheck.check({rest_id:'', order_id: ''}));
app.all('/getOrder', order.getOrder);

app.all('/getOrdersByRest',paramCheck.check({rest_id:''}, {date: 'number'}));
app.all('/getOrdersByRest', order.getOrdersByRest);

app.all('/addApn',paramCheck.check({user_id:'', apn: ''}));
app.all('/addApn', user.addApn);

app.all('/addRestApn',paramCheck.check({rest_id:'', apn: ''}));
app.all('/addRestApn', rest.addApn);

//these are for test use
app.all('/pushAll', user.pushAll);
app.all('/comment', order.addComment);
app.all('/update/:rest_id/:food_id', food.updateClick);

//below is all about call rest
app.all('/call/read/:rest_id/:order_id/:lang/:delay', call.read);
app.all('/call/option/:rest_id/:order_id/:lang/:delay', call.option);
app.all('/call/decide/:rest_id/:order_id/:lang/:delay', call.decide);

app.all('/call/confirm/:rest_id/:order_id/:lang/:delay', call.confirm);
app.all('/call/minute/:rest_id/:order_id/:lang/:delay', call.minute);

app.all('/call/cancel/:rest_id/:order_id/:lang/:delay', call.cancel);
app.all('/call/reason/:rest_id/:order_id/:lang/:delay', call.reason);

app.all('/call/callback/:rest_id/:order_id/:lang/:delay', call.callback);
app.all('/call/text', call.text);

app.all('/call/done/:rest_id/:order_id/:lang/:delay', call.done);

app.all('/call/test', call.test);
app.all('/call/status', call.status);

//phone check
app.all('/phone/:phone', user.checkPhone);

//get message
app.all('/push', order.push);
app.all('/message', order.getMessage);

//index
app.all('/', function(req, res, next){
	res.send(req.param('haha'));
});

//Error handlers
app.use(error.handle);


//--------------------------------

//run the server
console.log('starting server on 8080..');
http.createServer(app).listen(8080);
console.log(require('os').hostname());
console.log('server running on 8080..');
//https.createServer(options, app).listen(443);
